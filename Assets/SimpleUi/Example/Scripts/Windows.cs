﻿using SimpleUi.Example.Scripts.Controller;
using SimpleUi.Runtime.Main;
using SimpleUi.Runtime.Main.Impls;

namespace SimpleUi.Example.Scripts
{
	public class FirstUiWindow : UiWindow
	{
		protected override void AddControllers()
		{
			AddController<FirstController>();
		}
	}

	public class SecondUiWindow : UiWindow
	{
		protected override void AddControllers()
		{
			AddController<SecondController>();
		}
	}

	public class ThirdUiWindow : UiWindow
	{
		protected override void AddControllers()
		{
			AddController<ThirdController>();
		}
	}

	public class FirstPopUpUiWindow : UiWindow, INoneHidden
	{
		protected override void AddControllers()
		{
			AddController<FirstPopUpController>();
		}
	}

	public class SecondPopUpUiWindow : UiWindow
	{
		protected override void AddControllers()
		{
			AddController<SecondPopUpController>();
		}
	}

	public class FourthUiWindow : UiWindow
	{
		protected override void AddControllers()
		{
			AddController<FourthController>();
			AddController<FirstController>();
			AddController<SecondController>();
		}
	}

	public class FifthUiWindow : UiWindow
	{
		protected override void AddControllers()
		{
			AddController<FifthController>();
			AddController<FirstController>();
			AddController<SecondController>();
			AddController<ThirdController>();
		}
	}
}