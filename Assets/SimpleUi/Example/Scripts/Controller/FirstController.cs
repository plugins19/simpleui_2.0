﻿using SimpleUi.Example.Scripts.Views;
using SimpleUi.Runtime.Main.Impls;
using SimpleUi.Runtime.Main.WindowControllers;
using UniRx;
using Zenject;

namespace SimpleUi.Example.Scripts.Controller
{
	public class FirstController : UiController<FirstView>, IInitializable
	{
		private readonly ISceneWindowController _sceneWindowController;

		public FirstController(
			ISceneWindowController sceneWindowController
		)
		{
			_sceneWindowController = sceneWindowController;
		}

		public void Initialize()
		{
			View.Next.OnClickAsObservable()
				.Subscribe(_ => _sceneWindowController.OpenWindow<SecondUiWindow>())
				.AddTo(View.Next);
			View.Back.OnClickAsObservable().Subscribe(_ => _sceneWindowController.CloseWindow()).AddTo(View.Back);
		}
	}

	public class SecondController : UiController<SecondView>, IInitializable
	{
		private readonly ISceneWindowController _sceneWindowController;

		public SecondController(
			ISceneWindowController sceneWindowController
		)
		{
			_sceneWindowController = sceneWindowController;
		}

		public void Initialize()
		{
			View.Next.OnClickAsObservable()
				.Subscribe(_ => _sceneWindowController.OpenWindow<ThirdUiWindow>())
				.AddTo(View.Next);
			View.Back.OnClickAsObservable().Subscribe(_ => _sceneWindowController.CloseWindow()).AddTo(View.Back);
		}
	}

	public class ThirdController : UiController<ThirdView>, IInitializable
	{
		private readonly ISceneWindowController _sceneWindowController;

		public ThirdController(
			ISceneWindowController sceneWindowController
		)
		{
			_sceneWindowController = sceneWindowController;
		}

		public void Initialize()
		{
			View.Next.OnClickAsObservable()
				.Subscribe(_ => _sceneWindowController.OpenWindow<FirstPopUpUiWindow>("Hello"))
				.AddTo(View.Next);
			View.Back.OnClickAsObservable().Subscribe(_ => _sceneWindowController.CloseWindow()).AddTo(View.Back);
		}
	}

	public class FirstPopUpController : UiController<FirstPopUpView>, IInitializable
	{
		private readonly ISceneWindowController _sceneWindowController;

		public FirstPopUpController(
			ISceneWindowController sceneWindowController
		)
		{
			_sceneWindowController = sceneWindowController;
		}

		public void Initialize()
		{
			View.Next.OnClickAsObservable()
				.Subscribe(_ => _sceneWindowController.OpenWindow<SecondPopUpUiWindow>())
				.AddTo(View.Next);
			View.Back.OnClickAsObservable().Subscribe(_ => _sceneWindowController.CloseWindow()).AddTo(View.Back);
		}

		public override void Parametrize(object parameter) => View.Text.text = (string)parameter;
	}

	public class SecondPopUpController : UiController<SecondPopUpView>, IInitializable
	{
		private readonly ISceneWindowController _sceneWindowController;

		public SecondPopUpController(
			ISceneWindowController sceneWindowController
		)
		{
			_sceneWindowController = sceneWindowController;
		}

		public void Initialize()
		{
			View.Next.OnClickAsObservable()
				.Subscribe(_ => _sceneWindowController.OpenWindow<FourthUiWindow>())
				.AddTo(View.Next);
			View.Back.OnClickAsObservable().Subscribe(_ => _sceneWindowController.CloseWindow()).AddTo(View.Back);
		}
	}

	public class FourthController : UiController<FourthView>, IInitializable
	{
		private readonly ISceneWindowController _sceneWindowController;

		public FourthController(
			ISceneWindowController sceneWindowController
		)
		{
			_sceneWindowController = sceneWindowController;
		}

		public void Initialize()
		{
			View.Next.OnClickAsObservable()
				.Subscribe(_ => _sceneWindowController.OpenWindow<FifthUiWindow>())
				.AddTo(View.Next);
			View.Back.OnClickAsObservable().Subscribe(_ => _sceneWindowController.CloseWindow()).AddTo(View.Back);
		}
	}

	public class FifthController : UiController<FifthView>, IInitializable
	{
		private readonly ISceneWindowController _sceneWindowController;

		public FifthController(
			ISceneWindowController sceneWindowController
		)
		{
			_sceneWindowController = sceneWindowController;
		}

		public void Initialize()
		{
			View.Back.OnClickAsObservable().Subscribe(_ => _sceneWindowController.CloseWindow()).AddTo(View.Back);
		}
	}
}