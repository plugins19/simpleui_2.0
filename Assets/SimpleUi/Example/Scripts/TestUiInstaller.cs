﻿using SimpleUi.Runtime.Extensions;
using SimpleUi.Runtime.Main.WindowControllers.Impls;
using Zenject;

namespace SimpleUi.Example.Scripts
{
	public class TestUiInstaller : MonoInstaller
	{
		public override void InstallBindings()
		{
			Container.BindInterfacesAndSelfTo<FirstUiWindow>().AsSingle();
			Container.BindInterfacesAndSelfTo<SecondUiWindow>().AsSingle();
			Container.BindInterfacesAndSelfTo<ThirdUiWindow>().AsSingle();
			Container.BindInterfacesAndSelfTo<FourthUiWindow>().AsSingle();
			Container.BindInterfacesAndSelfTo<FifthUiWindow>().AsSingle();
			Container.BindInterfacesAndSelfTo<FirstPopUpUiWindow>().AsSingle();
			Container.BindInterfacesAndSelfTo<SecondPopUpUiWindow>().AsSingle();

			Container.BindInterfacesAndSelfTo<TestManager>().AsSingle().NonLazy();

			Container.BindWindowsController<SceneWindowController>();
		}
	}
}