﻿using SimpleUi.Runtime.Main.WindowControllers;
using Zenject;

namespace SimpleUi.Example.Scripts
{
	public class TestManager : IInitializable
	{
		private readonly ISceneWindowController _sceneWindowController;

		public TestManager(
			ISceneWindowController sceneWindowController
		)
		{
			_sceneWindowController = sceneWindowController;
		}

		public void Initialize()
		{
			_sceneWindowController.OpenWindow<FirstUiWindow>();
		}
	}
}