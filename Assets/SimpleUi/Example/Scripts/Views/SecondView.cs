﻿using SimpleUi.Runtime.Main.Impls;
using UnityEngine.UI;

namespace SimpleUi.Example.Scripts.Views
{
	public class SecondView : UiView
	{
		public Button Next;
		public Button Back;
	}
}