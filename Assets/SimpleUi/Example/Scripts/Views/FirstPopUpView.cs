﻿using SimpleUi.Runtime.Main.Impls;
using UnityEngine.UI;

namespace SimpleUi.Example.Scripts.Views
{
	public class FirstPopUpView : UiView
	{
		public Button Next;
		public Button Back;
		public Text Text;
	}
}