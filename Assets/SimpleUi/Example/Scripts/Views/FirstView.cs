﻿using SimpleUi.Runtime.Main.Impls;
using UnityEngine.UI;

namespace SimpleUi.Example.Scripts.Views
{
	public class FirstView : UiView
	{
		public Button Next;
		public Button Back;
	}
}