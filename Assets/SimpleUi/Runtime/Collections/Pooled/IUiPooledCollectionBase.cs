using SimpleUi.Runtime.Main;

namespace SimpleUi.Runtime.Collections.Pooled
{
	public interface IUiPooledCollectionBase<TView> : IUiCollectionBase<TView> where TView : IUiView
	{
		void Despawn(TView view);
	}

	public interface IUiPooledCollection<TView> : IUiPooledCollectionBase<TView>, IUiCollection<TView>
		where TView : IUiView
	{
	}

	public interface IUiPooledCollection<in TParam1, TView> : IUiPooledCollectionBase<TView>,
		IUiCollection<TParam1, TView>
		where TView : IUiView, IParametrizedView<TParam1>
	{
	}

	public interface IUiPooledCollection<in TParam1, in TParam2, TView> : IUiPooledCollectionBase<TView>,
		IUiCollection<TParam1, TParam2, TView>
		where TView : IUiView, IParametrizedView<TParam1, TParam2>
	{
	}

	public interface IUiPooledCollection<in TParam1, in TParam2, in TParam3, TView> : IUiPooledCollectionBase<TView>,
		IUiCollection<TParam1, TParam2, TParam3, TView>
		where TView : IUiView, IParametrizedView<TParam1, TParam2, TParam3>
	{
	}

	public interface IUiPooledCollection<in TParam1, in TParam2, in TParam3, in TParam4, TView> : IUiPooledCollectionBase<TView>,
		IUiCollection<TParam1, TParam2, TParam3, TParam4, TView>
		where TView : IUiView, IParametrizedView<TParam1, TParam2, TParam3, TParam4>
	{
	}

	public interface IUiPooledCollection<in TParam1, in TParam2, in TParam3, in TParam4, in TParam5, TView> :
		IUiPooledCollectionBase<TView>,
		IUiCollection<TParam1, TParam2, TParam3, TParam4, TParam5, TView>
		where TView : IUiView, IParametrizedView<TParam1, TParam2, TParam3, TParam4, TParam5>
	{
	}

	public interface IUiPooledCollection<in TParam1, in TParam2, in TParam3, in TParam4, in TParam5, in TParam6,
		TView> : IUiPooledCollectionBase<TView>,
		IUiCollection<TParam1, TParam2, TParam3, TParam4, TParam5, TParam6, TView>
		where TView : IUiView, IParametrizedView<TParam1, TParam2, TParam3, TParam4, TParam5, TParam6>
	{
	}

	public interface IUiPooledCollection<in TParam1, in TParam2, in TParam3, in TParam4, in TParam5, in TParam6, in TParam7,
		TView> : IUiPooledCollectionBase<TView>,
		IUiCollection<TParam1, TParam2, TParam3, TParam4, TParam5, TParam6, TParam7, TView>
		where TView : IUiView, IParametrizedView<TParam1, TParam2, TParam3, TParam4, TParam5, TParam6, TParam7>
	{
	}
}