using SimpleUi.Runtime.Main;

namespace SimpleUi.Runtime.Collections.Lists
{
	public interface IUiResizableListCollectionBase<TView> : IUiListCollectionBase<TView> where TView : IUiView
	{
		void Resize(int size);
	}
}