using SimpleUi.Runtime.Main;

namespace SimpleUi.Runtime.Collections.Lists
{
	public interface IUiStaticCollection<TView> : IUiListCollectionBase<TView> where TView : IUiView
	{
	}
}