namespace SimpleUi.Runtime.Collections.Uniques
{
	public interface IUniqueView<out TKey>
	{
		TKey Key { get; }
	}
}