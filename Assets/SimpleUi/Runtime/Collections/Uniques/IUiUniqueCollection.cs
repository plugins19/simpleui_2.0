using SimpleUi.Runtime.Main;

namespace SimpleUi.Runtime.Collections.Uniques
{
	public interface IUiUniqueCollectionBase<in TKey, out TView> : IUiCollectionBase<TView>
		where TView : IUiView, IUniqueView<TKey>
	{
		TView this[TKey key] { get; }

		void Remove(TKey key);

		bool Contains(TKey key);
	}

	public interface IUiUniqueCollection<in TKey, out TView> : IUiUniqueCollectionBase<TKey, TView>,
		IUiCollection<TKey, TView>
		where TView : IParametrizedView<TKey>, IUiView, IUniqueView<TKey>
	{
	}

	public interface
		IUiUniqueCollection<in TKey, in TParam1, out TView> : IUiUniqueCollectionBase<TKey, TView>,
			IUiCollection<TKey, TParam1, TView>
		where TView : IParametrizedView<TKey, TParam1>, IUiView, IUniqueView<TKey>
	{
	}

	public interface
		IUiUniqueCollection<in TKey, in TParam1, in TParam2, out TView> : IUiUniqueCollectionBase<TKey, TView>,
			IUiCollection<TKey, TParam1, TParam2, TView>
		where TView : IParametrizedView<TKey, TParam1, TParam2>, IUiView, IUniqueView<TKey>
	{
	}

	public interface
		IUiUniqueCollection<in TKey, in TParam1, in TParam2, in TParam3, out TView> : IUiUniqueCollectionBase<TKey, TView>,
			IUiCollection<TKey, TParam1, TParam2, TParam3, TView>
		where TView : IParametrizedView<TKey, TParam1, TParam2, TParam3>, IUiView, IUniqueView<TKey>
	{
	}

	public interface
		IUiUniqueCollection<in TKey, in TParam1, in TParam2, in TParam3, in TParam4, out TView> : IUiUniqueCollectionBase<TKey, TView>,
			IUiCollection<TKey, TParam1, TParam2, TParam3, TParam4, TView>
		where TView : IParametrizedView<TKey, TParam1, TParam2, TParam3, TParam4>, IUiView, IUniqueView<TKey>
	{
	}

	public interface
		IUiUniqueCollection<in TKey, in TParam1, in TParam2, in TParam3, in TParam4, in TParam5, out TView> : IUiUniqueCollectionBase<TKey,
				TView>,
			IUiCollection<TKey, TParam1, TParam2, TParam3, TParam4, TParam5, TView>
		where TView : IParametrizedView<TKey, TParam1, TParam2, TParam3, TParam4, TParam5>, IUiView, IUniqueView<TKey>
	{
	}

	public interface
		IUiUniqueCollection<in TKey, in TParam1, in TParam2, in TParam3, in TParam4, in TParam5, in TParam6, out TView> : IUiUniqueCollectionBase<TKey, TView>,
			IUiCollection<TKey, TParam1, TParam2, TParam3, TParam4, TParam5, TParam6, TView>
		where TView : IParametrizedView<TKey, TParam1, TParam2, TParam3, TParam4, TParam5, TParam6>, IUiView,
		IUniqueView<TKey>
	{
	}
}