using SimpleUi.Runtime.Main;

namespace SimpleUi.Runtime.Collections.Uniques
{
	public interface IUiUniqueStaticCollection<in TKey, out TView> : IUiUniqueCollectionBase<TKey, TView>
		where TView : IUniqueView<TKey>, IUiView
	{
	}
}