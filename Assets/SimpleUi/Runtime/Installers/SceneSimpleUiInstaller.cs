using SimpleUi.Runtime.Main.WindowControllers.Impls;
using UnityEngine;

namespace SimpleUi.Runtime.Installers
{
	[CreateAssetMenu(menuName = "Installers/SimpleUi/" + nameof(SceneSimpleUiInstaller),
		fileName = nameof(SceneSimpleUiInstaller), order = 0
	)]
	public class SceneSimpleUiInstaller : ASimpleUiInstaller<SceneWindowController>
	{
		
	}
}