using SimpleUi.Runtime.Main.WindowControllers.Impls;
using UnityEngine;

namespace SimpleUi.Runtime.Installers
{
	[CreateAssetMenu(menuName = "Installers/SimpleUi/" + nameof(ProjectSimpleUiInstaller),
		fileName = nameof(ProjectSimpleUiInstaller), order = 0
	)]
	public class ProjectSimpleUiInstaller : ASimpleUiInstaller<ProjectWindowController>
	{
		
	}
}