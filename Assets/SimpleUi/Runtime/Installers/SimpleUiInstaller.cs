using SimpleUi.Runtime.Extensions;
using SimpleUi.Runtime.Main.WindowControllers;
using Zenject;

namespace SimpleUi.Runtime.Installers
{
	public abstract class ASimpleUiInstaller<TWindowsController> : ScriptableObjectInstaller
		where TWindowsController : IWindowController
	{
		public override void InstallBindings()
		{
			Container.BindWindowsController<TWindowsController>();
		}
	}
}