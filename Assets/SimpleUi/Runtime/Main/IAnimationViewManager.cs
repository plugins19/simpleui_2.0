using System;
using UniRx;

namespace SimpleUi.Runtime.Main
{
	public interface IAnimationViewManager
	{
		IObservable<Unit> OpenAnimate(IAnimationView view);

		IObservable<Unit> CloseAnimate(IAnimationView view);
	}
}