namespace SimpleUi.Runtime.Main
{
	public interface IWindowLibrary
	{
		void Add(IUiWindow window);

		void Remove(IUiWindow window);
	}
}