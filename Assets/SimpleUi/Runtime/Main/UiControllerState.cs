﻿namespace SimpleUi.Runtime.Main
{
	public struct UiControllerState
	{
		public readonly bool IsActive;
		public readonly bool InFocus;
		public readonly int Order;
		
		public static readonly UiControllerState Default = new(false, false, 0);

		public UiControllerState(bool isActive, bool inFocus, int order)
		{
			IsActive = isActive;
			InFocus = inFocus;
			Order = order;
		}
	}
}