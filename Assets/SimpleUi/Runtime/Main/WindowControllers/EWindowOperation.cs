namespace Core.AnimSimpleUi
{
	public enum EWindowOperation : byte
	{
		None = 0,
		Opening = 1,
		Closing = 2
	}
}