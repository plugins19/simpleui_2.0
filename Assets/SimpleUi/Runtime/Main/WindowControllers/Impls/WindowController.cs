using System;
using System.Collections.Generic;
using System.Linq;
using Core.AnimSimpleUi;
using SimpleUi.Runtime.Extensions;
using SimpleUi.Runtime.Main.States.Impls;
using UniRx;
using Zenject;

namespace SimpleUi.Runtime.Main.WindowControllers.Impls
{
	public abstract class WindowController : IWindowController, IWindowLibrary, IDisposable
	{
		private const string OPEN_ROOT_WINDOW_OPERATION_NAME = "RootWindow";

		private readonly WindowState _windowState;
		private readonly IAnimationViewManager _animationViewManager;
		private readonly List<IUiWindow> _windows = new();
		private readonly Queue<WindowControllerOperation> _pendingOperations = new();

		private Stack<IUiWindow> _windowsStack = new();
		private WindowControllerOperation _currentOperation = WindowControllerOperation.Empty;
		private IDisposable _animateDisposable = Disposable.Empty;

		protected WindowController(
			[Inject(Source = InjectSources.Local)] WindowState windowState,
			[Inject(Source = InjectSources.Local)] IAnimationViewManager animationViewManager
		)
		{
			_windowState = windowState;
			_animationViewManager = animationViewManager;
		}

		public void Dispose() => _animateDisposable.Dispose();

		public void OpenWindow<TWindow>() where TWindow : IUiWindow => OpenWindow(typeof(TWindow).Name);

		public void OpenWindow(string windowName)
		{
			var newWindow = _windows.GetByName(windowName);
			var newOperation = new WindowControllerOperation(newWindow.Name, EWindowOperation.Opening);

			if (!IsAvailableToExecuteAndSaveIfNot(newOperation))
				return;

			_windowsStack.Push(newWindow);
			HandleWindowsInStack(newWindow);
			OpenWindowWithAnimation(newWindow);
		}

		public void OpenWindow<TWindow>(object parameter)
			where TWindow : IUiWindow => OpenWindow(typeof(TWindow).Name, parameter);

		public void OpenWindow(string windowName, object parameter)
		{
			var newWindow = _windows.GetByName(windowName);
			var newOperation = new WindowControllerOperation(newWindow.Name, EWindowOperation.Opening, parameter);

			if (!IsAvailableToExecuteAndSaveIfNot(newOperation))
				return;

			_windowsStack.Push(newWindow);

			foreach (var controller in newWindow.Controllers)
				controller.Parametrize(parameter);

			HandleWindowsInStack(newWindow);
			OpenWindowWithAnimation(newWindow);
		}

		public void CloseWindow()
		{
			var newOperation =
				new WindowControllerOperation(_windowState.CurrentWindowName.Value, EWindowOperation.Closing);

			if (!IsAvailableToExecuteAndSaveIfNot(newOperation))
				return;

			if (!_windowsStack.TryPop(out var lastWindow))
				return;

			var observables = new List<IObservable<Unit>>();

			foreach (var controller in lastWindow.Controllers)
			{
				if (controller is not IAnimationController animationController)
					continue;

				observables.Add(_animationViewManager.CloseAnimate(animationController.AnimationView));
			}

			void OnCloseAnimationFinished(Unit _)
			{
				_animateDisposable.Dispose();
				_currentOperation = WindowControllerOperation.Empty;

				foreach (var controller in lastWindow.Controllers)
					controller.SetState(UiControllerState.Default);

				if (!_windowsStack.TryPeek(out var currentWindow))
				{
					_windowState.SetWindowName(WindowState.EMPTY_WINDOW_NAME);
					return;
				}

				HandleWindowsInStack(currentWindow);
				_windowState.SetWindowName(currentWindow.Name);
				TryHandlePendingOperations();
			}

			_currentOperation = newOperation;
			_animateDisposable = observables.WhenAll().Subscribe(OnCloseAnimationFinished);
		}

		public void OpenRootWindow()
		{
			var newOperation = new WindowControllerOperation(OPEN_ROOT_WINDOW_OPERATION_NAME, EWindowOperation.Opening);

			if (!IsAvailableToExecuteAndSaveIfNot(newOperation))
				return;

			var windowsList = _windowsStack.ToList();
			var observables = new List<IObservable<Unit>>();
			var controllers = new List<IUiController>();

			for (var index = 1; index < windowsList.Count; index++)
			{
				var window = windowsList[index];
				controllers.AddRange(window.Controllers);
			}

			foreach (var controller in controllers)
			{
				if (controller is not IAnimationController animationController)
					continue;

				observables.Add(_animationViewManager.CloseAnimate(animationController.AnimationView));
			}

			void OnCloseAnimationFinished(Unit _)
			{
				_animateDisposable.Dispose();

				foreach (var controller in controllers)
					controller.SetState(UiControllerState.Default);

				_windowsStack = new Stack<IUiWindow>();
				_windowsStack.Push(windowsList[0]);
				HandleWindowsInStack(windowsList[0]);
				_windowState.SetWindowName(windowsList[0].Name);
				_currentOperation = WindowControllerOperation.Empty;
				TryHandlePendingOperations();
			}

			_currentOperation = newOperation;
			_animateDisposable = observables.WhenAll().Subscribe(OnCloseAnimationFinished);
		}

		public void Add(IUiWindow window) => _windows.Add(window);

		public void Remove(IUiWindow window) => _windows.Remove(window);

		private bool IsAvailableToExecuteAndSaveIfNot(WindowControllerOperation newOperation)
		{
			if (newOperation == _currentOperation)
				return false;

			if (_currentOperation != WindowControllerOperation.Empty)
			{
				_pendingOperations.Enqueue(newOperation);
				return false;
			}

			_currentOperation = newOperation;
			return true;
		}

		private void HandleWindowsInStack(IUiWindow currentWindow)
		{
			var currentWindowControllersCount = currentWindow.Controllers.Count;
			var openedControllers = _windowsStack.GetOpenedControllers(currentWindow);
			var closedControllers = _windowsStack.GetClosedControllers(openedControllers);

			foreach (var closedController in closedControllers)
				closedController.SetState(UiControllerState.Default);

			for (var index = 0; index < openedControllers.Count; index++)
			{
				var controller = openedControllers[index];
				var inFocus = index < currentWindowControllersCount;
				var newControllerState = new UiControllerState(true, inFocus, index);
				controller.SetState(newControllerState);
			}
		}

		private void OpenWindowWithAnimation(IUiWindow currentWindow)
		{
			var observables = new List<IObservable<Unit>>();

			foreach (var controller in currentWindow.Controllers)
			{
				if (controller is not IAnimationController animationController)
					continue;

				observables.Add(_animationViewManager.OpenAnimate(animationController.AnimationView));
			}

			void OnOpenAnimationFinished(Unit _)
			{
				_animateDisposable.Dispose();
				_currentOperation = WindowControllerOperation.Empty;
				_windowState.SetWindowName(currentWindow.Name);
				TryHandlePendingOperations();
			}

			_animateDisposable = observables.WhenAll().Subscribe(OnOpenAnimationFinished);
		}

		private void TryHandlePendingOperations()
		{
			if (!_pendingOperations.TryDequeue(out var newOperation))
				return;

			if (newOperation.Operation == EWindowOperation.Closing)
			{
				CloseWindow();
			}
			else if (newOperation is
			         { WindowName: OPEN_ROOT_WINDOW_OPERATION_NAME, Operation: EWindowOperation.Opening })
			{
				OpenRootWindow();
			}
			else if (newOperation is { Operation: EWindowOperation.Opening, Parameter1: { } })
			{
				OpenWindow(newOperation.WindowName, newOperation.Parameter1);
			}
			else if (newOperation is { Operation: EWindowOperation.Opening, Parameter1: null })
			{
				OpenWindow(newOperation.WindowName);
			}
		}
	}
}