using SimpleUi.Runtime.Main.States.Impls;

namespace SimpleUi.Runtime.Main.WindowControllers.Impls
{
	public class SceneWindowController : WindowController, ISceneWindowController
	{
		public SceneWindowController(
			WindowState windowState,
			IAnimationViewManager animationViewManager
		) : base(
			windowState, animationViewManager)
		{
		}
	}
}