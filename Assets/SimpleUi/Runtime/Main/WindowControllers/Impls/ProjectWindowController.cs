using SimpleUi.Runtime.Main.States.Impls;

namespace SimpleUi.Runtime.Main.WindowControllers.Impls
{
	public class ProjectWindowController : WindowController, IProjectWindowController
	{
		public ProjectWindowController(
			WindowState windowState,
			IAnimationViewManager animationViewManager
		) : base(windowState, animationViewManager)
		{
		}
	}
}