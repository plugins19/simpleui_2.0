using System;

namespace Core.AnimSimpleUi
{
	public readonly struct WindowControllerOperation
	{
		public override bool Equals(object obj) => obj is WindowControllerOperation other && Equals(other);

		public override int GetHashCode() => HashCode.Combine(WindowName, (int)Operation);

		public readonly string WindowName;
		public readonly EWindowOperation Operation;
		public readonly object Parameter1;

		public static WindowControllerOperation Empty = new(string.Empty, EWindowOperation.None);

		public WindowControllerOperation(string windowName, EWindowOperation operation)
		{
			WindowName = windowName;
			Operation = operation;
			Parameter1 = null;
		}

		public WindowControllerOperation(
			string windowName,
			EWindowOperation operation,
			object parameter1
		) : this()
		{
			WindowName = windowName;
			Operation = operation;
			Parameter1 = parameter1;
		}

		public static bool operator ==(WindowControllerOperation first, WindowControllerOperation second)
			=> first.WindowName == second.WindowName && first.Operation == second.Operation;

		public static bool operator !=(WindowControllerOperation first, WindowControllerOperation second)
			=> first.WindowName != second.WindowName || first.Operation != second.Operation;

		public bool Equals(WindowControllerOperation other)
			=> WindowName == other.WindowName && Operation == other.Operation;
	}
}