namespace SimpleUi.Runtime.Main.WindowControllers
{
	public interface IWindowController
	{
		void OpenWindow<TWindow>() where TWindow : IUiWindow;

		void OpenWindow(string windowName);

		void OpenWindow<TWindow>(object parameter)
			where TWindow : IUiWindow;

		void OpenWindow(string windowName, object parameter);

		void CloseWindow();

		void OpenRootWindow();
	}
}