using System.Collections.Generic;

namespace SimpleUi.Runtime.Main
{
	public interface IUiWindow
	{
		string Name { get; }
		
		List<IUiController> Controllers { get; }
	}
}