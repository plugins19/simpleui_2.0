using UniRx;

namespace SimpleUi.Runtime.Main.States
{
	public interface IWindowState
	{
		IReadOnlyReactiveProperty<string> CurrentWindowName { get; }
	}
}