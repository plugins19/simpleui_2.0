using UniRx;

namespace SimpleUi.Runtime.Main.States.Impls
{
	public class WindowState : IWindowState
	{
		public const string EMPTY_WINDOW_NAME = "EmptyWindow";

		private readonly StringReactiveProperty _currentWindowName = new(EMPTY_WINDOW_NAME);
		public IReadOnlyReactiveProperty<string> CurrentWindowName => _currentWindowName;

		public void SetWindowName(string windowName) => _currentWindowName.Value = windowName;
	}
}