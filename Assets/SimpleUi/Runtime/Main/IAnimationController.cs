namespace SimpleUi.Runtime.Main
{
	public interface IAnimationController
	{
		IAnimationView AnimationView { get; }
	}
}