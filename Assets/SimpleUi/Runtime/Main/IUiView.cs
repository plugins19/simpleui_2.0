﻿using UnityEngine;

namespace SimpleUi.Runtime.Main
{
	public interface IUiView
	{
		bool IsShow { get; }

		void Show();
		
		void Hide();

		void SetOrder(int index);
		
		void SetParent(Transform parent);
		
		void Destroy();
	}
}