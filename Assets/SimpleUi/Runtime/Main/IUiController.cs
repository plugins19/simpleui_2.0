﻿namespace SimpleUi.Runtime.Main
{
	public interface IUiController
	{
		void SetState(UiControllerState state);

		void Parametrize(object parameter);
	}
}