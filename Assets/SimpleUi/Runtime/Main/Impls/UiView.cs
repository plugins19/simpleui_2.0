﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace SimpleUi.Runtime.Main.Impls
{
	public abstract class UiView : UIBehaviour, IUiView
	{
		public bool IsShow { get; private set; }

		public void Show()
		{
			gameObject.SetActive(true);
			IsShow = true;
			OnShow();
		}

		protected virtual void OnShow()
		{
		}

		public void Hide()
		{
			gameObject.SetActive(false);
			IsShow = false;
			OnHide();
		}

		protected virtual void OnHide()
		{
		}

		public void SetParent(Transform parent) => transform.SetParent(parent, false);

		public void SetOrder(int index)
		{
			var childCount = transform.parent.childCount - 1;
			transform.SetSiblingIndex(childCount - index);
		}

		public void Destroy() => Destroy(gameObject);
	}
}