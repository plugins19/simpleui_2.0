﻿using Zenject;

namespace SimpleUi.Runtime.Main.Impls
{
	public abstract class UiController<T> : IUiController
		where T : IUiView
	{
		private bool _prevIsActive;
		private bool _prevInFocus;
		
		[Inject] protected readonly T View;

		public void SetState(UiControllerState state)
		{
			if (state.IsActive)
				View.SetOrder(state.Order);

			if (_prevIsActive != state.IsActive)
			{
				_prevIsActive = state.IsActive;
				OnActive(state.IsActive);
			}

			if (_prevInFocus != state.InFocus)
			{
				_prevInFocus = state.InFocus;
				OnHasFocus(state.InFocus);
			}
		}

		public virtual void Parametrize(object parameter)
		{
		}

		private void OnActive(bool isActive)
		{
			if (isActive)
			{
				View.Show();
				OnShow();
				return;
			}
			
			View.Hide();
			OnHide();
		}

		protected virtual void OnShow() { }

		protected virtual void OnHide() { }

		protected virtual void OnHasFocus(bool inFocus) { }
	}
}