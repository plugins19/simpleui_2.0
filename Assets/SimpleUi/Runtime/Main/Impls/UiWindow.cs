using System;
using System.Collections.Generic;
using Zenject;

namespace SimpleUi.Runtime.Main.Impls
{
	public abstract class UiWindow : IUiWindow, IInitializable, IDisposable
	{
		public string Name => GetType().Name;

		public List<IUiController> Controllers { get; } = new();

		[Inject] private DiContainer _container;
		[Inject(Source = InjectSources.Local)] private IWindowLibrary _windowLibrary;

		public void Initialize() => _windowLibrary.Add(this);

		public void Dispose() => _windowLibrary.Remove(this);

		[Inject]
		protected abstract void AddControllers();

		protected void AddController<TController>()
			where TController : IUiController
		{
			var controller = _container.Resolve<TController>();
			Controllers.Add(controller);
		}
	}
}