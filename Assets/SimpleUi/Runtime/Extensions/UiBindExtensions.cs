﻿using SimpleUi.Runtime.Main;
using SimpleUi.Runtime.Main.States.Impls;
using SimpleUi.Runtime.Main.WindowControllers;
using UnityEngine;
using Zenject;

namespace SimpleUi.Runtime.Extensions
{
	public static class UiBindExtensions
	{
		public static void BindUiView<T, TU>(this DiContainer container, Object viewPrefab, Transform parent)
			where TU : IUiView
			where T : IUiController
		{
			container.BindInterfacesAndSelfTo<T>().AsSingle();
			container.BindInterfacesAndSelfTo<TU>()
				.FromComponentInNewPrefab(viewPrefab)
				.UnderTransform(parent).AsSingle()
				.OnInstantiated((_, o) => ((MonoBehaviour) o).gameObject.SetActive(false));
		}

		public static void BindWindowsController<TWindowController>(this DiContainer container)
			where TWindowController : IWindowController
		{
			container.BindInterfacesTo<TWindowController>().AsSingle().NonLazy();
			var windowState = new WindowState();
			container.BindInterfacesTo<WindowState>().FromInstance(windowState).AsSingle();
			container.Bind<WindowState>().FromInstance(windowState).WhenInjectedInto<TWindowController>();
		}
	}
}