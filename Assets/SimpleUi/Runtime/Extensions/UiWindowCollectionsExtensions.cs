using System;
using System.Collections.Generic;
using SimpleUi.Runtime.Main;

namespace SimpleUi.Runtime.Extensions
{
	public static class UiWindowCollectionsExtensions
	{
		public static IUiWindow GetByName(this List<IUiWindow> windows, string windowName)
		{
			foreach (var window in windows)
			{
				if (window.Name != windowName)
					continue;

				return window;
			}

			throw new Exception(
				$"[ {nameof(UiWindowCollectionsExtensions)} ] You haven't the window with name: {windowName}");
		}

		public static List<IUiController> GetOpenedControllers(
			this Stack<IUiWindow> windowsStack,
			IUiWindow currentWindow
		)
		{
			var controllers = new List<IUiController>();
			controllers.AddRange(currentWindow.Controllers);

			foreach (var window in windowsStack)
			{
				if (window is not INoneHidden)
					continue;

				foreach (var controller in window.Controllers)
				{
					if (controllers.Contains(controller))
						continue;

					controllers.Add(controller);
				}
			}

			return controllers;
		}

		public static List<IUiController> GetClosedControllers(
			this Stack<IUiWindow> windowsStack,
			List<IUiController> openedControllers
		)
		{
			var controllers = new List<IUiController>();

			foreach (var window in windowsStack)
			{
				if (window is INoneHidden)
					continue;

				foreach (var controller in window.Controllers)
				{
					if (openedControllers.Contains(controller))
						continue;
					
					controllers.Add(controller);
				}
			}

			return controllers;
		}
	}
}